from random import randint

from igraph import Graph


class PercolationGraph:
    def __init__(self, side=10, edges_cnt=3, width=1024, height=768, vertex_size=28, random_edges=False):
        self.__side = side
        self.__width = width
        self.__height = height
        self.__vertex_size = vertex_size
        self.__site_graph = None
        self.__total_vertices = side * side

        self.__igraph = self.__get_graph(edges_cnt, random_edges)

    def get_igraph(self):
        return self.__igraph

    def get_vertices_count(self):
        return self.__total_vertices

    def get_side(self):
        return self.__side

    def __get_graph(self, edges_cnt, random_edges):
        g = Graph()

        for vertex in range(self.__total_vertices):
            g.add_vertex(vertex, label=vertex, color='yellow', size=self.__vertex_size)

        for vertex in range(self.__total_vertices):
            if random_edges:
                edges = self.__get_random_edges(vertex, edges_cnt)
            else:
                edges = self.__get_edges(vertex)
            g.add_edges(edges)

        g.es['width'] = 5

        return g

    def __get_random_edges(self, vertex, edges_cnt):
        edges = []

        while len(edges) < edges_cnt:
            edge = self.__get_edge(vertex)
            if edge[0] != edge[1] and edge not in edges and edge[::-1] not in edges:
                edges.append(edge)

        return edges

    def __get_edge(self, start_vertex):
        end_vertex = randint(0, self.__total_vertices - 1)
        return start_vertex, end_vertex

    def __get_edges(self, vertex):
        edges = []

        if (vertex + 1) % self.__side != 0:
            edges.append((vertex, vertex + 1))
        if vertex + self.__side < self.__total_vertices:
            edges.append((vertex, vertex + self.__side))

        return edges