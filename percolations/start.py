from datetime import datetime

from percolations.Percolation import SitePercolation, BondPercolation
from percolations.PercolationGraph import PercolationGraph
from percolations.Plot import GridPlot


def test():
    graph = PercolationGraph(side=40, vertex_size=10)

    plot = GridPlot(800, 600)
    plot.plot_graph(graph)

    # percolation = SitePercolation()
    percolation = BondPercolation()

    percolation.percolation(graph, .65)
    plot.plot_graph(graph)

    percolation.compute_long_range_connectivity()
    plot.plot_graph(graph)

start = datetime.now()
test()
print(datetime.now() - start)

# bond
# 65  | .55
# site
# 60  | .66
# 103 | .64

# percolation treshold:
# http://en.wikipedia.org/wiki/Percolation_threshold