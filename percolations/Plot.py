from igraph import plot


class Plot:
    def __init__(self, width=1024, height=768):
        self.__width = width
        self.__height = height

    def plot_graph(self, graph, file_name=None):
        g = graph.get_igraph()
        layout = self.get_layout(g)
        self.plot(g, file_name, layout)

    def plot(self, graph, file_name, layout=None):
        bbox = (0, 0, self.__width, self.__height)
        if layout is None:
            plot(graph, target=file_name, bbox=bbox)
        else:
            plot(graph, target=file_name, layout=layout, bbox=bbox)

    def get_layout(self, graph):
        pass


class CirclePlot(Plot):
    def get_layout(self, graph):
        return graph.layout_circle(2)


class GridPlot(Plot):
    def get_layout(self, graph):
        return graph.layout_grid()


class ReingoldTilfordStarPlot(Plot):
    def get_layout(self, graph):
        return graph.layout_reingold_tilford_circular()