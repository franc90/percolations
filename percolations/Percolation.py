from random import uniform

from igraph import Graph


class Percolation:
    def __init__(self):
        self.__side = None
        self.__total_vertices = None
        self.__igraph = None

    def get_side(self):
        return self.__side

    def get_vertices_cnt(self):
        return self.__total_vertices

    def get_igraph(self):
        return self.__igraph

    def percolation(self, graph, p):
        self.__side = graph.get_side()
        self.__total_vertices = graph.get_vertices_count()
        self.__igraph = graph.get_igraph()

    def get_graph(self):
        pass

    @staticmethod
    def random_boolean(p):
        val = uniform(0, 1)
        return val < p

    def compute_long_range_connectivity(self, do_plot=True):
        shortest_path = self.__get_shortest_path(do_plot)
        if do_plot:
            self.__color_graph(shortest_path)
        return len(shortest_path) > 0

    def __get_shortest_path(self, do_plot):
        g = self.get_graph()
        shortest_path = []

        for v1 in self.__get_start_vertices(g):
            paths = []
            for v2 in self.__get_end_vertices(g):
                path = g.get_shortest_paths(v1, v2)[0]
                if len(path) > 0:
                    if do_plot:
                        paths.append(path)
                    else:
                        return path

            if len(paths) > 0:
                paths.sort(key=lambda s: len(s))
                if len(shortest_path) == 0 or len(shortest_path) > len(paths[0]):
                    shortest_path = paths[0]

        shortest_path = self.__get_shortest_paths_names(shortest_path, g)
        return shortest_path

    @staticmethod
    def __get_shortest_paths_names(shortest_path, g):
        sp = []
        for v in shortest_path:
            sp.append(g.vs[v]['name'])
        return sp

    def __color_graph(self, shortest_path):
        if len(shortest_path) == 0:
            return

        g = self.__igraph
        for v in shortest_path:
            g.vs[v]['color'] = 'red'
        for i in range(len(shortest_path) - 1):
            g.es[g.get_eid(shortest_path[i], shortest_path[i + 1])]['color'] = 'red'
            g.es[g.get_eid(shortest_path[i], shortest_path[i + 1])]['width'] = 8

    def __get_start_vertices(self, g):
        vertices = []
        for v in g.vs:
            if v['name'] < self.__side:
                vertices.append(v)
            else:
                return vertices
        return vertices

    def __get_end_vertices(self, g):
        vertices = []
        for v in g.vs[::-1]:
            if v['name'] >= self.__total_vertices - self.__side:
                vertices.append(v)
            else:
                return vertices
        return vertices


class BondPercolation(Percolation):
    def percolation(self, graph, p):
        super().percolation(graph, p)

        g = self.get_igraph()
        for edge in g.get_edgelist():
            if not Percolation.random_boolean(p):
                g.delete_edges(edge)

    def get_graph(self):
        return self.get_igraph()


class SitePercolation(Percolation):
    def __init__(self):
        super().__init__()
        self.__site_graph = None

    def get_graph(self):
        return self.__site_graph

    def percolation(self, graph, p):
        super().percolation(graph, p)

        g = self.get_igraph()
        self.__site_graph = self.__copy_graph(g)
        for vertex_name in range(graph.get_vertices_count() - 1, 0, -1):
            if not Percolation.random_boolean(p):
                g.vs[vertex_name]['color'] = "black"
                self.__site_graph.delete_vertices(vertex_name)

    def __copy_graph(self, graph):
        g = Graph()

        for vertex in range(self.get_vertices_cnt()):
            g.add_vertex(vertex, label=vertex)
        g.add_edges(graph.get_edgelist())

        return g